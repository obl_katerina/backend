<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRelations extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('adverts', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('category_id')->references('id')->on('sub_categories');
        });

        Schema::table('sub_categories', function (Blueprint $table){
            $table->foreign('main_category_id')->references('id')->on('main_categories');
        });

        Schema::table('comments', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('advert_id')->references('id')->on('adverts');
        });

        Schema::table('favorites', function (Blueprint $table){
            $table->foreign('user_id')->references('id')->on('users');
            $table->foreign('advert_id')->references('id')->on('adverts');
        });

        Schema::table('metrics', function (Blueprint $table){
            $table->foreign('advert_id')->references('id')->on('adverts');
        });

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
