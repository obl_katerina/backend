<?php
/**
 * Created by PhpStorm.
 * User: malut
 * Date: 14.07.2018
 * Time: 16:45
 */

interface HomePets
{
    public function makeASound();
    public function toEat();
}