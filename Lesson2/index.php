<?php

require 'Cat.php';
require 'Parrot.php';
require 'Hamster.php';

$catMax = new Cat("Max", "grey");
$parrotAnton = new Parrot("Anton", "blue");
$hamsterDavid = new Hamster("David", "black");

$catMax->makeASound();
$catMax->setSize(2.1);
$catMax->toEat();
echo "  ";
echo $catMax->getSize() . "kg";

$parrotAnton->makeASound();
$hamsterDavid->makeASound();
