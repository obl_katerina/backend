<?php
/**
 * Created by PhpStorm.
 * User: malut
 * Date: 14.07.2018
 * Time: 16:48
 */

class Hamster implements HomePets
{

    private $name;
    private $size;
    private $color;

    public function __construct($name, $color)
    {
        $this->name = $name;
        $this->color = $color;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setName($name)
    {
        $this->name = $name;
    }

    public function getSize()
    {
        return $this->size;
    }

    public function setSize($size)
    {
        $this->size = $size;
    }

    public function getColor()
    {
        return $this->color;
    }

    public function setColor($color)
    {
        $this->color = $color;
    }

    public function makeASound()
    {
	echo "<br>";
        echo "Pi-pi-pi-pi";
    }

    public function toEat()
    {
        $this->size += 0.1;
    }
}
